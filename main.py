def main():
    print("Uneti elemente prve liste:\n")
    lst1 = list(map(int, input().split()))
    print("Uneti elemente druge liste:\n")
    lst2 = list(map(int, input().split()))

    for ele1 in lst1:
        num_div = 0
        for ele2 in lst2:
            if ele1 % ele2 == 0:
                num_div += 1
        if num_div >= 3:
            print(ele1)
        else:
            print("Element '{0}' iz prve list:'{1}' nije deljive sa najmanje tri elementa druge liste:'{2}'"
                  .format(ele1, lst1, lst2))

if __name__ == "__main__":
    main()
